import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

import { BsDropdownModule } from 'ngx-bootstrap/dropdown'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { AppRoutingModule } from 'app/app-routing.module'

import { Directive1Directive } from 'app/directives/directive1/directive1.directive'
import { LoadingDirective } from 'app/directives/loading/loading.directive'
import { ValidationDirective } from 'app/directives/validation/validation.directive'

import { NullishCoalescingPipe } from 'app/pipes/nullish-coalescing/nullish-coalescing.pipe'
import { CutStringPipe } from 'app/pipes/cut-string/cut-string.pipe'

import { AppComponent } from 'app/app.component'
import { NavbarComponent } from 'app/shared/navbar/navbar.component'
import { Comp1Component } from 'app/components/home/comp1/comp1.component'
import { Comp2Component } from 'app/components/home/comp2/comp2.component'
import { HomeComponent } from 'app/components/home/home.component'
import { DataComponent } from 'app/data/data.component'
import { SignUpComponent } from 'app/components/signup/sign-up.component'
import { SignInComponent } from 'app/components/signin/sign-in.component'
import { ProfileComponent } from 'app/components/profile/profile.component'

@NgModule({
  declarations: [
    AppComponent,
    Comp1Component,
    Comp2Component,
    NullishCoalescingPipe,
    CutStringPipe,
    Directive1Directive,
    LoadingDirective,
    ValidationDirective,
    HomeComponent,
    SignUpComponent,
    SignInComponent,
    ProfileComponent,
    DataComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    FontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
