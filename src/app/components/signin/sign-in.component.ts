import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { UserService } from '../../services/user/user.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-signin',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  form: FormGroup
  isLoggingIn: boolean = false

  constructor(
    private fb: FormBuilder,
    private service: UserService,
    private router: Router
  ) {
    this.form = this.createFormData()
  }

  ngOnInit(): void {}

  createFormData(): FormGroup {
    return this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  onLogIn(): void {
    this.form.get('username').markAsTouched()
    this.form.get('password').markAsTouched()

    if (this.form.invalid) return

    const { username, password } = this.form.value
    this.isLoggingIn = true
    this.service.login(username, password).subscribe(
      (isLogInSucceed) => {
        if (isLogInSucceed) alert(`✔ Successfully: คุณเข้าสู่ระบบสำเร็จ!`)
        this.isLoggingIn = false
        this.router.navigate(['/', 'profile']).then()
      },
      (error) => {
        alert(`❌ Failed: ${error.message}`)
        this.isLoggingIn = false
      }
    )
  }
}
