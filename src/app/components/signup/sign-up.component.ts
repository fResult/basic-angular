import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { UserService } from '../../services/user/user.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-signup',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  form: FormGroup
  isConfirmed: boolean = false
  isLoading: boolean = false

  constructor(
    private fb: FormBuilder,
    private service: UserService,
    private router: Router
  ) {
    this.createFormData()
  }

  ngOnInit(): void {}

  private createFormData(): void {
    this.form = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: [
        '',
        [Validators.required, Validators.pattern(/^[A-z0-9]{8,16}$/)]
      ],
      password: ['', Validators.required],
      isConfirmed: [false]
    })
  }

  async onRegister(): Promise<void> {
    this.form.get('firstName').markAsTouched()
    this.form.get('lastName').markAsTouched()
    this.form.get('username').markAsTouched()
    this.form.get('password').markAsTouched()
    if (this.form.invalid) return

    this.isLoading = true

    const { firstName, lastName, username, password } = this.form.value
    const res = await this.service.register({
      id: Math.floor(Math.random() * 100),
      firstName,
      lastName,
      username,
      password
    })
    if (!res?.error) {
      alert(`✔ Successfully: ลงทะเบียนผู้ใช้ ${username} สำเร็จ! `)
      this.router.navigate(['/', 'sign-in']).then()
    } else {
      alert(res.error)
    }
    this.isLoading = false
  }
}
