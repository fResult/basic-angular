import { Component, OnInit } from '@angular/core'
import { Service2Service } from '../../services/service2.service'
import { Album } from '../../interfaces/basic-http-client/album'
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  albums: Array<Album> = []

  constructor(private service2: Service2Service, private http: HttpClient) {}
  ngOnInit(): void {
    this.service2.fetchAlbumsFromNodeJsServer().subscribe((albums) => {
      this.albums = albums
    })
  }
}
