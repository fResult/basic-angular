import { Component, OnInit } from '@angular/core'
import { Service2Service } from 'app/services/service2.service'
import { IFormData } from 'app/interfaces/IFormData'

@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.scss']
})
export class Comp1Component implements OnInit {
  formData: IFormData

  constructor(private service2: Service2Service) {}

  async ngOnInit(): Promise<void> {
    try {
      this.formData = await this.service2.getPromiseFormData()
    } catch (err) {
      console.error(err.message)
    }
  }
}
