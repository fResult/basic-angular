import { Component } from '@angular/core'
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms'
import { Service2Service } from 'app/services/service2.service'

@Component({
  selector: 'app-comp2',
  templateUrl: './comp2.component.html',
  styleUrls: ['./comp2.component.scss']
})
export class Comp2Component {
  form: FormGroup
  isLoading: boolean = false

  constructor(private builder: FormBuilder, private service2: Service2Service) {
    this.createFormData()
  }

  async onSubmit(): Promise<void> {
    this.form.get('sex').markAsTouched()
    this.form.get('firstName').markAsTouched()
    this.form.get('lastName').markAsTouched()
    this.getPhoneFields().controls.forEach((control) => {
      control.markAsTouched()
    })
    if (this.form.invalid) {
      alert('INVALID FORM')
      return
    }

    try {
      this.isLoading = true
      await this.service2.onPromiseSubmitFormData(this.form.value)
    } catch (err) {
      alert(`❌ Error: ${err.message}`)
    } finally {
      this.isLoading = false
    }
  }

  onAddPhoneField(): void {
    const phoneFieldArray: FormArray = this.getPhoneFields()
    phoneFieldArray.push(this.createPhoneControl())
  }

  onRemovePhoneField(): void {
    const phoneFieldArray = this.getPhoneFields()
    if (phoneFieldArray.length <= 1) {
      return
    }
    phoneFieldArray.removeAt(phoneFieldArray.length - 1)
  }

  getPhoneFields(): FormArray {
    return this.form.get('phones') as FormArray
  }

  private createFormData(): void {
    this.form = this.builder.group({
      sex: [null, Validators.required],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      phones: this.builder.array([this.createPhoneControl()])
    })
  }

  private createPhoneControl(): AbstractControl {
    return this.builder.control(null, [
      Validators.required,
      Validators.pattern(/^[0-9]{10}$/)
    ])
  }
}
