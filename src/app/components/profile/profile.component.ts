import { Component, OnInit } from '@angular/core'
import { UserService } from '../../services/user/user.service'
import { IUser } from '../../interfaces/IUser'
import { Router } from '@angular/router'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, Deactivate {
  user: IUser
  isLoggingOut: boolean

  constructor(private service: UserService, private router: Router) {
    this.initialLoadUser()
  }

  ngOnInit(): void {}

  onExit(): boolean {
    return confirm('Are you sure to exit this page ?')
  }

  private initialLoadUser(): void {
    this.service.getUserLoggedIn().subscribe((user) => {
      this.user = user
    })
  }

  onLogOut(): void {
    this.isLoggingOut = true
    this.service.clearUserLoggedIn().subscribe(() => {
      this.isLoggingOut = false
      this.router.navigate(['/', 'sign-in']).then()
    })
  }
}
