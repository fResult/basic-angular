import {
  Directive,
  Input,
  OnChanges,
  TemplateRef,
  ViewContainerRef,
  SimpleChanges
} from '@angular/core'

@Directive({
  selector: '[appLoading]'
})
export class LoadingDirective implements OnChanges {
  @Input()
  appLoading: boolean

  constructor(
    private templateRef: TemplateRef<boolean>,
    private containerRef: ViewContainerRef
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.appLoading)
      this.containerRef.createEmbeddedView(this.templateRef)
    else
      this.containerRef.remove()
  }
}
