import {
  Directive,
  HostBinding,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core'
import { AbstractControl } from '@angular/forms'

@Directive({
  selector: '[appValidation]'
})
export class ValidationDirective implements OnChanges {
  constructor() {}
  @HostBinding()
  class: string = 'invalid-feedback'

  @HostBinding()
  innerText: string

  @Input('appValidation')
  control: AbstractControl

  errorMessages = {
    required: 'Field is required!',
    pattern: 'Field does not match {pattern}',
    message: ''
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.control) {
      return
    }

    // When Submit before touch input field
    this.innerText = this.showValidationMessageOnInvalidField(this.control)

    // When any input is changed
    this.control.valueChanges.subscribe(() => {
      this.innerText = this.showValidationMessageOnInvalidField(this.control)
    })
  }

  showValidationMessageOnInvalidField(formControl: AbstractControl): string {
    const isInvalid = formControl && formControl.invalid
    if (isInvalid) {
      const errorKey = Object.keys(formControl.errors)[0]
      let errorMsg: string = this.errorMessages[errorKey]

      switch (errorKey) {
        case 'pattern':
          errorMsg = errorMsg.replace(
            '{pattern}',
            formControl.errors.pattern.requiredPattern
          )
          break
        case 'message':
          errorMsg = formControl.errors[errorKey]
          break
        default:
          break
      }
      return errorMsg
    }
  }
}
