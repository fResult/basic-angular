import { Injectable } from '@angular/core'
import { IFormData } from '../interfaces/IFormData'
import { Observable, Subject } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { Album } from '../interfaces/basic-http-client/album'
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class Service2Service {
  data: string = 'Data from Service2'
  private formData: IFormData = new FormData()
  formDataSubject: Subject<IFormData> = new Subject()

  constructor(private http: HttpClient) {}

  fetchAlbums(): Observable<Array<Album>> {
    return this.http.get<Array<Album>>(
      'https://jsonplaceholder.typicode.com/albums'
    )
  }

  fetchAlbumsFromPhpServer(): Observable<Array<Album>> {
    return this.http.get<Array<Album>>('http://localhost:8080/server.php')
  }

  fetchAlbumsFromNodeJsServer(): Observable<Array<Album>> {
    return this.http.get<Array<Album>>('http://localhost:3000/albums')
  }

  private onSubmitFormData(formData: IFormData): void {
    // Cannot save data like this
    // this.formData = { ...formData }
    // because this.formData will be changed data type

    this.formData.sex = formData.sex
    this.formData.firstName = formData.firstName
    this.formData.lastName = formData.lastName
    this.formData.phones = formData.phones
  }

  onObserveSubmitFormData(formData: IFormData): Observable<any> {
    return new Observable((observer) => {
      setTimeout(() => {
        if (
          formData.firstName.toLowerCase() === 'test' ||
          formData.lastName.toLowerCase() === 'test'
        ) {
          return observer.error({ message: 'Cannot type any "test" word' })
        }
        this.onSubmitFormData(formData)
        observer.next()
      }, 1500)
    })
  }

  getPromiseFormData(): Promise<FormData> {
    return new Promise<IFormData>((resolve, reject) => {
      setTimeout(() => {
        resolve(this.formData)
      }, 1500)
      // reject({ message: 'Error na kub!' })
    })
  }

  onPromiseSubmitFormData(formData: FormData): Promise<void> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (
          formData.firstName.toLowerCase() === 'test' ||
          formData.lastName.toLowerCase() === 'test'
        ) {
          return reject({ message: 'Cannot type any "test" word' })
        }
        this.onSubmitFormData(formData)
        this.formDataSubject.next(formData)
        resolve()
      }, 1500)
    })
  }
}

class FormData implements IFormData {
  sex: string
  firstName: string
  lastName: string
  phones: Array<string>
}
