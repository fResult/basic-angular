import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { IUser } from '../../interfaces/IUser'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: IUser[] = []
  userLoggedIn: IUser
  endpoint: string = 'http://localhost:3000/users'

  constructor(private http: HttpClient) {}

  async register(user: IUser): Promise<any> {
    if (await this.checkIsUsernameExist(user.username)) {
      return { error: `⚠ มีผู้ใช้ ${user.username} ในระบบแล้ว` }
    } else {
      try {
        await this.http.post(this.endpoint, user).toPromise()
        return { message: 'Success' }
      } catch (err) {
        return { error: err }
      }
    }
  }

  login(username: string, password: string): Observable<boolean> {
    return new Observable<boolean>((observer) => {
      setTimeout(() => {
        this.userLoggedIn = this.users.find((u) => username === u.username)
        this.checkLogInSucceed(username, password)
          ? observer.next(true)
          : observer.error(new Error('Username หรือ Password ไม่ถูกต้อง'))
      }, 1500)
    })
  }

  getUsers(): Observable<Array<IUser>> {
    return new Observable<Array<IUser>>((observer) => {
      setTimeout(() => {
        observer.next(
          this.users.map((user) => ({
            ...user,
            password: '•'.repeat(user.password.length)
          }))
        )
      }, 1500)
    })
  }

  getUserLoggedIn(): Observable<IUser> {
    return new Observable<IUser>((observer) => {
      setTimeout(() => {
        observer.next(this.userLoggedIn)
      }, 0)
    })
  }

  clearUserLoggedIn(): Observable<void> {
    return new Observable((observer) => {
      setTimeout(() => {
        this.userLoggedIn = null
        observer.next()
      }, 1500)
    })
  }

  async checkIsUsernameExist(username): Promise<boolean> {
    const response = await this.http
      .get<{ user: IUser }>(`${this.endpoint}/${username}`)
      .toPromise()

    return !!response.user
  }

  checkLogInSucceed(username, password): boolean {
    return this.users.some((u) => {
      return u.username === username && u.password === password
    })
  }
}
