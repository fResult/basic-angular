import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { HomeComponent } from 'app/components/home/home.component'
import { SignInComponent } from 'app/components/signin/sign-in.component'
import { SignUpComponent } from 'app/components/signup/sign-up.component'
import { ProfileComponent } from 'app/components/profile/profile.component'
import { DataComponent } from 'app/data/data.component'
import { AuthenticatedGuard } from './guards/authentication/authenticated-guard.service'
import { UnauthenticatedGuard } from './guards/authentication/unauthenticated-guard.service'

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'sign-up', component: SignUpComponent, canActivate: [UnauthenticatedGuard] },
  { path: 'sign-in', component: SignInComponent, canActivate: [UnauthenticatedGuard] },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthenticatedGuard]
  },
  { path: 'data', component: DataComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule {}
