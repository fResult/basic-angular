import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'cutString'
})
export class CutStringPipe implements PipeTransform {
  transform(value: unknown, limit: number = 50): unknown {
    return this.onCutMessage(value as string, limit)
  }

  onCutMessage(message: string, limit: number): string {
    console.log('NUMBER', limit)
    return message.length > (limit)
      ? `${message.substr(0, limit)} [...]`
      : message
  }
}
