import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import db from './services/db/firebase.db'
import { r, g, b, w, c, m, y, k } from './services/log-color.service'
import UserRouter from './routes/user.router'

const server = express()
const PORT = 3000

server.use(cors(''))
server.use(bodyParser.urlencoded({ extended: false }))
server.use(bodyParser.json())

server.get('/', (req, res) => {
  res.send({ message: 'Hello world!' })
})

server.get('/albums', (req, res) => {
  const albums = [
    { id: 10001, title: 'omnis laborum odio', userId: 'KornZilla01' },
    {
      id: 10002,
      title: 'quibusdam autem aliquid et et quia',
      userId: 'KornZilla02'
    },
    {
      id: 10003,
      title: 'ut pariatur rerum ipsum natus repellendus praesentium',
      userId: 'KornZilla03'
    },
    {
      id: 10004,
      title:
        'repudiandae voluptatem optio est consequatur rem in temporibus et	',
      userId: 'KornZilla04'
    },
    {
      id: 10005,
      title:
        'modi consequatur culpa aut quam soluta alias perspiciatis laudantium',
      userId: 'KornZilla05'
    }
  ]

  res.json(albums)
})

UserRouter(server, db)

server.listen(PORT, () => {
  console.log(
    `${c('[INFO]')} Server is started with ${'http://localhost:' + PORT}`
  )
})

export default server
