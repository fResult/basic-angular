import { Storage } from '@google-cloud/storage'

const storage = new Storage()
async function listBuckets() {
  try {
    const results = await storage.getBuckets()

    const [buckets] = results

    console.log('Buckets:')
    buckets.forEach((bucket) => {
      console.log(bucket.name)
    })
  } catch (err) {
    console.error('ERROR:', err)
  }
}

export default listBuckets
