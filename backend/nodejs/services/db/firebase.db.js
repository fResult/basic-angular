import admin from 'firebase-admin'
import serviceAccount from '../../../../../GCP/basic-angular-auth-293608-0617801b655f.json'
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  apiKey: 'AIzaSyAMG-O2aNkAKNKHnG-1mDhlyUyzX8GxnF0',
  authDomain: 'basic-angular-auth-293608.firebaseapp.com',
  databaseURL: 'https://basic-angular-auth-293608.firebaseio.com',
  projectId: 'basic-angular-auth-293608',
  storageBucket: 'basic-angular-auth-293608.appspot.com',
  messagingSenderId: '1013334699521',
  appId: '1:1013334699521:web:13990cd0fb0e587d099a00',
  measurementId: 'G-49KMB56WHK'
})

const db = admin.firestore()

export default db
