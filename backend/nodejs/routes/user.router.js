const UserRouter = (app, db) => {
  const collection = db.collection('users')

  app.get('/users', async (req, res) => {
    res.json(db.docs('users').get())
  })

  app.get('/users/:username', async (req, res) => {
    try {
      const user = await collection.doc(req.params.username).get()
      res.json({ user: user.data() })
    } catch (err) {
      res.json({ error: err })
    }
  })

  app.post('/users', async (req, res) => {
    const docRef = collection.doc(req.body.username)

    try {
      await docRef.set(req.body)
      res.json({ message: 'Register Successfully.' })
    } catch (err) {
      res.json({ error: 'Wrong: ' + err })
    }
  })
}

export default UserRouter
