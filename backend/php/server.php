<?php
header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');

$items = [
  array(
    'id'      => 10001,
    'title'   => 'omnis laborum odio',
    'userId'  => 'KornZilla1'
  ),
  array(
    'id'      => 10002,
    'title'   => 'quibusdam autem aliquid et et quia',
    'userId'  => 'KornZilla2'
  ),
  array(
    'id'      => 10003,
    'title'   => 'ut pariatur rerum ipsum natus repellendus praesentium',
    'userId'  => 'KornZilla3'
  ),
  array(
    'id'      => 10004,
    'title'   => 'repudiandae voluptatem optio est consequatur rem in temporibus et	',
    'userId'  => 'KornZilla4'
  ),
  [
    'id'      => 10005,
    'title'   => 'modi consequatur culpa aut quam soluta alias perspiciatis laudantium	',
    'userId'  => 'KornZilla5'
  ]
];

echo json_encode($items);
